#ifndef CODE_GEN_H_
#define CODE_GEN_H_

#include <stack>
#include <map>
#include <string>
#include<memory>

#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"



namespace miao {

using namespace std;
using namespace llvm;
using namespace llvm::sys;

class NBlock;

static LLVMContext my_context;

class CodeGenBlock {
public:
    shared_ptr<BasicBlock> block;
    shared_ptr<Value> return_value;
    map<string, shared_ptr<Value>> locals;

};

class CodeGenContext {
private:
    stack<shared_ptr<CodeGenBlock>> blocks;
    shared_ptr<Function> main_function;

public:
    shared_ptr<Module> module;
    CodeGenContext() {
        module = shared_ptr<Module>(new Module("main", my_context));
    }

    void generate_code(NBlock& root);
    GenericValue run_code();
    map<string, shared_ptr<Value>>& locals() {
        return blocks.top()->locals;
    }

    shared_ptr<BasicBlock>  current_block() {
        return blocks.top()->block;
    }

    void push_block(shared_ptr<BasicBlock> block) {
        blocks.push(new CodeGenBlock());
        blocks.top()->return_value.reset();
        blocks.top()->block = block;
    }

    void pop_block() {
        auto top = blocks.top();
        blocks.pop();
    }

    void set_current_return_value(shared_ptr<Value> value) {
        blocks.top()->return_value = value;
    }

    shared_ptr<Value> get_current_return_value() {
        return blocks.top()->return_value;
    }
};


}

#endif // CODE_GEN_H_