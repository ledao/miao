%{

#include<string>
#include<iostream>

using namespace std;

extern int yylex();
void yyerror(const char *s) { std::printf("Error: %s\n", s);std::exit(1); }
extern "C" {
int yywrap() {
    return 0;
}
}


%}

%union {
    std::string* str;
    double f64;
}


%token tok_eol
%token tok_eof
%token tok_def
%token tok_extern
%token <str> tok_identifier
%token <f64> tok_number
%token tok_if 
%token tok_else
%token tok_for
%token tok_in
%token tok_var
%token tok_val
%token tok_binary_op
%token tok_colon
%token tok_l_curve
%token tok_r_curve
%token tok_l_brace
%token tok_r_brace
%token tok_comma
%token tok_assign


%start program

%%

program: stmts { cout << "program begin" << endl; }
;

stmts: stmt { }
| stmts stmt {  }
;

stmt: var_expr
| def_expr
;

var_expr: tok_var tok_identifier tok_colon tok_identifier tok_eol { 
    cout << "var define here, " << *$2 << endl;
}
;

def_expr: tok_def tok_identifier tok_l_curve def_args tok_r_curve {
    cout << "function name is " << *$2 << endl;
}
;

def_args: {}
    | def_arg {
        cout << "def args 1" << endl;
    }
    | def_args tok_comma def_arg {
        cout << "def args 2 " << endl;
    }
;

def_arg: tok_identifier tok_colon tok_identifier {
    cout << "def arg name is " << *$1 << ", type is " << *$3 << endl; 
}
;

%%