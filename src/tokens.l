%{

#include <string>

#include "parse.hpp"

using namespace std;

#define SAVE_TOKEN  yylval.str = new std::string(yytext, yyleng)

%}

%%

"extern"                      return tok_extern;
"def"                         return tok_def;
"if"                          return tok_if;
"else"                        return tok_else;
"for"                         return tok_for;
"in"                          return tok_in;
"var"                         return tok_var;
"val"                        return tok_val;
"+"                           return tok_binary_op;
"-"                           return tok_binary_op;
"*"                           return tok_binary_op;
"/"                           return tok_binary_op;
"%"                          return tok_binary_op;
":"                           return tok_colon;

"("                         return tok_l_curve;
")"                         return tok_r_curve;
"{"                         return tok_l_brace;
"}"                         return tok_r_brace;

,                           return tok_comma;
=                           return tok_assign;

[a-zA-Z_][a-zA-Z0-9_]*      SAVE_TOKEN; return tok_identifier;
[0-9]+                     SAVE_TOKEN; return tok_number;


[ \t\r]                     ; 
[\n]                        ;

%%