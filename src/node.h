#ifndef NODE_H_
#define NODE_H_

#include <string>
#include <vector>
#include <memory>
#include <map>

#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/TargetRegistry.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"


namespace miao
{
using namespace std;

using namespace llvm;
using namespace llvm::sys;

class CodeGenContext;
class Statement;
class Expression;
class VariableExpr;

typedef vector<unique_ptr<Statement>> StatementList;
typedef vector<unique_ptr<Expression>> ExpressionList;
typedef vector<unique_ptr<VariableExpr>> VariableList;


class Node {
public:
    virtual ~Node() {}
    virtual shared_ptr<Value> code_gen() {}
};

class Expression : public Node {};

class Statement : public Node {};

//数字表达式，例如"23.9"
class Float64Expr : public Expression {
private:
    double val;

public:
    Float64Expr(double val): val(val) {}
    shared_ptr<Value> codegen() override;
};

class Int64Expr : public Expression {
private:
    int64_t val;

public:
    Int64Expr(int64_t val): val(val) {}

    shared_ptr<Value> codegen() override;
};

class IdentifierExpr : public Expression {
public:
    string name;
    IdentifierExpr(const string& name): name(name) {}

    virtual shared_ptr<Value> code_gen() override;
};

class MethodCallExpr : public Expression {
public:
    unique_ptr<IdentifierExpr> name;
    ExpressionList arguments;
    MethodCallExpr(unique_ptr<IdentifierExpr> name, ExpressionList arguments)
        : name(name), arguments(arguments) {}

    MethodCallExpr(unique_ptr<IdentifierExpr> name): name(name) {}

    shared_ptr<Value> code_gen() override;
};


class BinaryOperatorExpr : public Expression {
private:
    string op;
    unique_ptr<Expression> lhs, rhs;

public:
    BinaryOperatorExpr(const string& op, unique_ptr<ExprAst> lhs, 
                    unique_ptr<ExprAst> rhs): op(op), lhs(lhs), rhs(rhs){}

    shared_ptr<Value> codegen() override;
};

//FIXME: here to go

class UnaryExprAst : public ExprAst {
private:
    string opcode;
    unique_ptr<ExprAst> operand;

public:
    UnaryExprAst(const string& opcode, unique_ptr<ExprAst> operand): opcode(opcode), operand(operand) {}

    Value* codegen() override;
};


class CallExprAst : public ExprAst {
private:
    string callee;
    vector<unique_ptr<ExprAst>> args;

public:
    CallExprAst(const string& callee,
                vector<unique_ptr<ExprAst>> args): callee(callee), args(args){}
    Value* codegen() override;

};

calss IfExprAst : public ExprAst {
private:
    unique_ptr<ExprAst> cond, then, else_;

public:
    IfExprAst(unique_ptr<ExprAst> cond, 
                unique_ptr<ExprAst> then,
                unique_ptr<ExprAst> else_)
                : cond(cond), then(then), else_(else_) {}

    Value* codegen() override;

};

class ForExprAst : public ExprAst {

};

class VarExprAst : public ExprAst {

};

class PrototypeAst
{
private:
    string name;
    vector<string> args;
    bool is_operator;
    unsigned int precedence;
public:
    PrototypeAst(const string& name, vector<string> args, 
                    bool is_operator = false, unsigned int prec = 0)
        : name(name), args(args), is_operator(is_operator), precedence(prec) {}

    Function* codegen();
    const string& get_name() {return name;}

    bool is_unary_op() {
        return is_operator && args.size() == 1;
    }
    bool is_binary_op() {
        return is_operator && args.size() == 2;
    }

    char get_operator_name() const {
        assert(is_unary_op() || is_binary_op());
        return name[name.size() - 1];
    }

    unsigned int get_binary_precedence() {
        return precedence;
    }
};


class FunctionAst {
private:
    unique_ptr<PrototypeAst> proto;
    unique_ptr<ExprAst> body;

public:
    FunctionAst(unique_ptr<PrototypeAst> proto, 
                unique_ptr<ExprAst> body)
        : proto(move(proto)), body(move(body)){}

    Function* codegen();
}



} // namespace miao


#endif // NODE_H_