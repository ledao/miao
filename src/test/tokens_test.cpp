#include <iostream>
#include <vector>

#include "../tokens.h"

using namespace std;
using namespace miao;

int main() {
    Lexer lexer;
    vector<TokenEle> tokens;
    lexer.parse_file("/home/ledao/projects/cpp_workspace/miao/src/test/tokens_test.mo", tokens);
    for (auto& p : tokens) {
        cout << p.identifier_str << ", " << p.token_id << endl;
    }
    // for (auto p = tokens.begin(); p != tokens.end(); p++) {
    //     cout << p->identifier_str << endl;
    // }
}