
set_languages("c99", "cxx11")

add_links("boost_regex")

target("tokens_test")
    set_kind("binary")
    add_files("tokens_test.cpp")
    add_files("../tokens.cpp")
