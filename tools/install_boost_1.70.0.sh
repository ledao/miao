#!/usr/bin/bash

if [ ! -f boost_1_70_0.tar.gz ]; then
    wget https://dl.bintray.com/boostorg/release/1.70.0/source/boost_1_70_0.tar.gz
fi

tar xzvf boost_1_70_0.tar.gz 

cd boost_1_70_0
chmod +x bootstrap.sh

sh bootstrap.sh --prefix=../boost
./b2 --no-cmake-config link=static install

rm -rf ../boost_1_70_0

echo "all done"
