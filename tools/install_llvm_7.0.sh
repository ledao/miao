#!/usr/bin/bash

if [ ! -f llvm-7.0.0.src.tar.xz ]; then
    wget http://releases.llvm.org/7.0.0/llvm-7.0.0.src.tar.xz
fi
tar xf llvm-7.0.0.src.tar.xz
rm -rf llvm
mv -f llvm-7.0.0.src llvm

if [ ! -f cfe-7.0.0.src.tar.xz ]; then
    wget http://releases.llvm.org/7.0.0/cfe-7.0.0.src.tar.xz
fi
tar xf cfe-7.0.0.src.tar.xz
rm -rf llvm/tools/clang
mv -f cfe-7.0.0.src llvm/tools/clang

if [ ! -f clang-tools-extra-7.0.0.src.tar.xz ]; then
    wget http://releases.llvm.org/7.0.0/clang-tools-extra-7.0.0.src.tar.xz
fi
tar xf clang-tools-extra-7.0.0.src.tar.xz
rm -rf llvm/tools/clang/tools/extra
mv -f clang-tools-extra-7.0.0.src llvm/tools/clang/tools/extra

if [ ! -f compiler-rt-7.0.0.src.tar.xz ]; then
    wget http://releases.llvm.org/7.0.0/compiler-rt-7.0.0.src.tar.xz
fi
tar xf compiler-rt-7.0.0.src.tar.xz
rm -rf llvm/projects/compiler-rt
mv -f compiler-rt-7.0.0.src llvm/projects/compiler-rt

cd llvm
rm -rf build
mkdir -p build 
cd build
mkdir -p ~/llvm-7
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=~/llvm-7 ..
make clean
make -j 4
make install 

cd ../..

echo "all done"
