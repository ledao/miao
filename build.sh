#!/usr/bin/bash

cd src
sh generate_parser.sh
cd ..

# Compile
clang++ -g -O3 src/main.cpp src/parse.cpp src/tokens.cpp `llvm-config --cxxflags --ldflags --system-libs --libs all` -o miao


exit 0